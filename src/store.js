import { configureStore } from '@reduxjs/toolkit'
import cartReducer from './Slice/cartSlice'
import favoritReducer from './Slice/favoritSlice'
import modalReducer from './Slice/modalSlice'

const store = configureStore({
  reducer: {
    cart: cartReducer,
    favorit: favoritReducer,
    modal: modalReducer,
  },
})

export default store
