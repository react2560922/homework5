import '../style.scss'
import { useSelector } from 'react-redux'
import { selectCart } from '../Slice/cartSlice'
import ProductsList from '../components/ProductList'
import OrderForm from '../components/OrderForm'

function ProductsCart({ id }) {
  const cartCounter = useSelector(selectCart)

  return (
    <>
      {cartCounter.length === 0 ? (
        <p className="modal__body-text">Товар відсутній</p>
      ) : (
        <div className="card-list">
          <ProductsList products={cartCounter} id={id} />
          <OrderForm productsList={cartCounter} />
        </div>
      )}
    </>
  )
}

export default ProductsCart
