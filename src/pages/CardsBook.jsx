import { useState, useEffect } from 'react'
import { useDispatch } from 'react-redux'
import ProductsList from '../components/ProductList'

import { createAsyncThunk } from '@reduxjs/toolkit'

function CardsBook() {
  const [products, setProducts] = useState([])
  const dispatch = useDispatch()

  const fetchProducts = createAsyncThunk('products/fetchProducts', async () => {
    const response = await fetch('./products.json')
    const data = await response.json()
    return data
  })

  useEffect(() => {
    dispatch(fetchProducts()).then((action) => {
      setProducts(action.payload)
    })
  }, [])

  return <ProductsList products={products} />
}

export default CardsBook
