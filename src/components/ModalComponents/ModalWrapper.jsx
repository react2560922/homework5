import './ModalWrapper.scss'

function ModalWrapper({ children, onClick }) {
  return (
    <div className="modal__wrapper" onClick={onClick}>
      {children}
    </div>
  )
}
export default ModalWrapper
