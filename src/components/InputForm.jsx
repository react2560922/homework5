function InputForm({ tag, name, typeElemForm, formik }) {
  const FormTag = tag

  return (
    <>
      <FormTag
        {...(typeElemForm && { type: typeElemForm })}
        name={name}
        id={name}
        onChange={formik.handleChange}
        onBlur={formik.handleBlur}
        value={formik.values[name]}
      />
      {formik.touched[name] && formik.errors[name] && (
        <div className="errors">{formik.errors[name]}</div>
      )}
    </>
  )
}

export default InputForm
