import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  isOpen: false,
  typeModal: '',
}

const modalSlice = createSlice({
  name: 'modal',
  initialState,
  reducers: {
    openModal: (state, action) => {
      state.isOpen = action.payload.isOpen
      state.typeModal = action.payload.typeModal
    },
    closeModal: (state) => {
      state.isOpen = false
      state.typeModal = ''
    },
  },
})

export const { openModal, closeModal } = modalSlice.actions

export const getModal = (state) => state.modal

export default modalSlice.reducer
