import { createSlice } from '@reduxjs/toolkit'

const initialState = JSON.parse(localStorage.getItem('card')) || []

const cartSlice = createSlice({
  name: 'cart',
  initialState,
  reducers: {
    setCart: (state, action) => {
      state.push(action.payload)
      localStorage.setItem('card', JSON.stringify(state))
    },
    deleteCart: (state, action) => {
      const updatedState = state.filter((item) => item.sku !== action.payload)
      localStorage.setItem('card', JSON.stringify(updatedState))
      return updatedState
    },
    orderUser: (state, action) => {
      localStorage.removeItem('card')
      return state.filter((item) => {
        item.sku !== action.payload
      })
    },
  },
})

export const { setCart, deleteCart, orderUser } = cartSlice.actions

export const selectCart = (state) => state.cart

export default cartSlice.reducer
