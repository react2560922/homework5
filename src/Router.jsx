import { createBrowserRouter } from 'react-router-dom'
import Layout from './Layout'
import CardsBook from './pages/CardsBook'
import ProductsCart from './pages/ProductsCart'
import ProductsFavorites from './pages/ProductsFavorites'

export const Router = createBrowserRouter([
  {
    path: '/',
    element: <Layout />,
    children: [
      {
        index: true,
        element: <CardsBook />,
      },
      {
        path: '/cart',
        element: <ProductsCart id="1" />,
      },
      {
        path: '/favorites',
        element: <ProductsFavorites id="2" />,
      },
    ],
  },
])
