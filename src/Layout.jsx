import { Outlet } from 'react-router-dom'

import { useState } from 'react'
import Header from './components/Header'

import Footer from './components/Footer'

import './style.scss'

function Layout() {
  return (
    <>
      <Header />

      <Outlet />

      <Footer />
    </>
  )
}

export default Layout
